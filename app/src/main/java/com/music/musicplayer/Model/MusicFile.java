package com.music.musicplayer.Model;

public class MusicFile {

    private String Album;
    private String Artiest;
    private String Duration;
    private String Path;
    private String Title;


    public MusicFile() {
    }

    public MusicFile(String album, String artiest, String duration, String path, String title) {
        Album = album;
        Artiest = artiest;
        Duration = duration;
        Path = path;
        Title = title;
    }

    public String getAlbum() {
        return Album;
    }

    public void setAlbum(String album) {
        Album = album;
    }

    public String getArtiest() {
        return Artiest;
    }

    public void setArtiest(String artiest) {
        Artiest = artiest;
    }

    public String getDuration() {
        return Duration;
    }

    public void setDuration(String duration) {
        Duration = duration;
    }

    public String getPath() {
        return Path;
    }

    public void setPath(String path) {
        Path = path;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }
}
