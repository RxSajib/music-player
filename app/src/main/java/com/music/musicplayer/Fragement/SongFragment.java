package com.music.musicplayer.Fragement;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.music.musicplayer.Adapter.MusicAdapter;
import com.music.musicplayer.MainActivity;
import com.music.musicplayer.R;

import static com.music.musicplayer.MainActivity.music_file;

public class SongFragment extends Fragment {

    private RecyclerView recyclerView;
    private MusicAdapter musicAdapter;

    public SongFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_song, container, false);

        recyclerView = view.findViewById(R.id.MusicList);


        if(!(music_file.size() < 1)){
            musicAdapter = new MusicAdapter(getContext(), music_file);
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            recyclerView.setAdapter(musicAdapter);
        }

        return view;
    }
}