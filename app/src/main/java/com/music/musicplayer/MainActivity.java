package com.music.musicplayer;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.android.material.tabs.TabLayout;
import com.music.musicplayer.Fragement.AlbumFragment;
import com.music.musicplayer.Adapter.TabpagerAdapter;
import com.music.musicplayer.Fragement.SongFragment;
import com.music.musicplayer.Model.MusicFile;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private TabpagerAdapter tabpagerAdapter;
    private String Permission[] = {Manifest.permission.READ_EXTERNAL_STORAGE};
    public static final int PermissionCode = 1;
     public static ArrayList<MusicFile> music_file ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //01923844167


        read_userpermission();


    }

    private void inislizepage(){
        tabLayout = findViewById(R.id.TablayoutID);
        viewPager = findViewById(R.id.ViewPagerID);

        tabpagerAdapter = new TabpagerAdapter(getSupportFragmentManager());
        tabpagerAdapter.addFragementPage(new SongFragment(), DataManager.Songs);
        tabpagerAdapter.addFragementPage(new AlbumFragment(), DataManager.Album);

        viewPager.setAdapter(tabpagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void read_userpermission(){
        if(ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(MainActivity.this, Permission, PermissionCode);
        }
        else {
            music_file = getadd_audio(getApplicationContext());
            inislizepage();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(requestCode == PermissionCode){
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED){
                music_file = getadd_audio(getApplicationContext());
            }
            else {
                ActivityCompat.requestPermissions(MainActivity.this, Permission, PermissionCode);
                inislizepage();
            }
        }
    }


    public static ArrayList<MusicFile> getadd_audio(Context context){
        ArrayList<MusicFile> file = new ArrayList<>();

        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;

        String[] projection = {
                MediaStore.Audio.Media.ALBUM,
                MediaStore.Audio.Media.TITLE,
                MediaStore.Audio.Media.DURATION,
                MediaStore.Audio.Media.ARTIST,
                MediaStore.Audio.Media.DATA
        };

        Cursor cursor = context.getContentResolver().query(uri, projection, null, null, null);

        if(cursor != null){
            while (cursor.moveToNext()){
                String Album = cursor.getString(0);
                String Title = cursor.getString(1);
                String Duration = cursor.getString(2);
                String Artist = cursor.getString(3);
                String Path = cursor.getString(4);



                MusicFile musicFile = new MusicFile(Album, Artist, Duration, Path, Title);
                file.add(musicFile);
            }

            cursor.close();
        }

        return file;
    }
}