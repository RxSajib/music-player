package com.music.musicplayer.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.material.textview.MaterialTextView;
import com.music.musicplayer.Model.MusicFile;
import com.music.musicplayer.R;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class MusicAdapter extends RecyclerView.Adapter<MusicAdapter.MusicHolder> {

    private Context context;
    private ArrayList<MusicFile> musicFileList;

    public MusicAdapter(Context context, ArrayList<MusicFile> musicFileList) {
        this.context = context;
        this.musicFileList = musicFileList;

    }

    @NonNull
    @Override
    public MusicHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View Mview = LayoutInflater.from(parent.getContext()).inflate(R.layout.music_iteams, parent, false);

        return new MusicHolder(Mview);
    }

    @Override
    public void onBindViewHolder(@NonNull MusicHolder holder, int position) {
        holder.title.setText(musicFileList.get(position).getTitle());
        holder.artist.setText(musicFileList.get(position).getArtiest());

        byte[] imagepath = getAmbumArt(musicFileList.get(position).getPath());
        if(imagepath != null){
            Glide.with(holder.imageView.getContext()).asBitmap()
                    .load(imagepath)
                    .into(holder.imageView);
        }


    }

    @Override
    public int getItemCount() {
        return musicFileList.size();
    }

    public class MusicHolder extends RecyclerView.ViewHolder{

        private CircleImageView imageView;
        private MaterialTextView title, artist;

        public MusicHolder(@NonNull View itemView) {
            super(itemView);


            imageView = itemView.findViewById(R.id.MusicImage);
            title = itemView.findViewById(R.id.MisicTitleID);
            artist = itemView.findViewById(R.id.MusicArtist);
        }


    }

    private byte[] getAmbumArt(String uri){
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();

        try {
            retriever.setDataSource(uri);
        }catch (Exception e){
            e.printStackTrace();
        }

        byte[] art = retriever.getEmbeddedPicture();
        retriever.release();
        return art;
    }


}
